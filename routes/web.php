<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','WelcomeController@index');

Route::get('/categories.php','WelcomeController@categories');

Route::get('/product_details.php','WelcomeController@product_details');

Route::get('/events.php','WelcomeController@events');

Route::get('/about_us.php','WelcomeController@about_us');

Route::get('/services.php','WelcomeController@services');

Route::get('/contact.php','WelcomeController@contact');

Route::get('/signin.php','WelcomeController@signin');

Route::get('/signup.php','WelcomeController@signup');
