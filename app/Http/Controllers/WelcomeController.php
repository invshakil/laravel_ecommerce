<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index(){
        return view('frontEnd.homeContent.homeContent');
    }

    public function categories(){
        return view('frontEnd.categories.categories');
    }

    public function product_details(){
        return view('frontEnd.pages.singleProduct');
    }

    public function events(){
        return view('frontEnd.pages.events');
    }

    public function about_us(){
        return view('frontEnd.pages.aboutUS');
    }

    public function services(){
        return view('frontEnd.pages.services');
    }

    public function contact(){
        return view('frontEnd.pages.contact');
    }

    public function signin(){
        return view('frontEnd.userEnd.logIn.login');
    }

    public function signup(){
        return view('frontEnd.userEnd.signUp.signup');
    }
}
