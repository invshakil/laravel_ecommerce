@extends('frontEnd.master')

@section('title')
    Sign Up || SmartShop
@endsection

@section('content')
    <div class="w3l_banner_nav_right">
        <!-- login -->
        <div class="w3_login">
            <h3>Sign Up</h3>
            <div class="w3_login_module">
                <div class="module form-module">
                    <div class="toggle"><i class="fa fa-times fa-pencil"></i></div>
                    <div class="form">
                        <h2>Enter your information for Sign Up</h2>
                        <form action="#" method="post">
                            <input type="text" name="Username" placeholder="Username" required=" ">
                            <input type="password" name="Password" placeholder="Password" required=" ">
                            <input type="email" name="Email" placeholder="Email Address" required=" ">
                            <input type="text" name="Phone" placeholder="Phone Number" required=" ">
                            <input type="submit" value="Register">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- //login -->
    </div>
    <div class="clearfix"></div>
@endsection