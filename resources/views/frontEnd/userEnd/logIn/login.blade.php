@extends('frontEnd.master')

@section('title')
    Login || SmartShop
@endsection

@section('content')
    <div class="w3l_banner_nav_right">
        <!-- login -->
        <div class="w3_login">
            <h3>Please Sign in to your account!</h3>
            <div class="w3_login_module">
                <div class="module form-module">
                    <div class="toggle"><i class="fa fa-times fa-pencil"></i>
                        {{--<div class="tooltip">Click Me</div>--}}
                    </div>
                    <div class="form">
                        <h2>Enter your login information</h2>
                        <form action="#" method="post">
                            <input type="text" name="Username" placeholder="Username" required=" ">
                            <input type="password" name="Password" placeholder="Password" required=" ">
                            <input type="submit" value="Login">
                        </form>
                    </div>
                    <div class="cta"><a href="#">Forgot your password?</a></div>
                </div>
            </div>
        </div>
        <!-- //login -->
    </div>
    <div class="clearfix"></div>
@endsection