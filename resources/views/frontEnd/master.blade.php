<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    @include('frontEnd.includes.header')
</head>

<body>
<!-- header -->
@include('frontEnd.includes.topNav')
<!-- //header -->

<!--Banner-->
@include('frontEnd.includes.banner')
<!-- //banner -->

<!--Nav Menu -->
<div class="banner">
    @include('frontEnd.includes.navBar')
</div>
<!--//Nav Menu -->

<!--Content-->


@yield('content')

<!--Content-->

<!-- newsletter -->
@include('frontEnd.includes.subscribe')

<!-- //newsletter -->
<!-- footer -->
@include('frontEnd.includes.footer')
<!-- //footer -->
<!-- Bootstrap Core JavaScript -->
@include('frontEnd.includes.footerScript')
</body>
</html>