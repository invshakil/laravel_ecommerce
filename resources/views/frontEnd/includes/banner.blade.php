<div class="logo_products">
    <div class="container">
        <div class="w3ls_logo_products_left">
            <h1><a href="{{URL::to('/')}}"><span>Accessories</span> Store</a></h1>
        </div>
        <div class="w3ls_logo_products_left1">
            <ul class="special_items">
                <li><a href="{{URL::to('/events.php')}}">Events</a><i>/</i></li>
                <li><a href="{{URL::to('/about_us.php')}}">About Us</a><i>/</i></li>
                <li><a href="{{URL::to('/services.php')}}">Services</a></li>
            </ul>
        </div>
        <div class="w3ls_logo_products_left1">
            <ul class="phone_email">
                <li><i class="fa fa-phone" aria-hidden="true"></i>(+0123) 234 567</li>
                <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:store@grocery.com">store@grocery
                        .com</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>